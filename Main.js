/**
 * Javascript is single-threaded, it can only do one thing at one time,
 * it relies on APIs within its environment to do tasks for it and then 
 * jump on queue (the Event Loop) once those tasks are complete
 * 
 * 
 */

const {getDataFromServer,getDataWithCallback} = require('./AsyncFunctions')

const Main = () => {
    const data = getDataFromServer()
    console.log(data)
}

const MainWithCallbacks = () =>{

    const callThisWhenDone = (data) =>{
        console.log(data)
    }

    getDataWithCallback(callThisWhenDone)
}

const MainWithPromises = () =>{
    const data = getDataFromServer().then(result=>{
        console.log(result)
    }).catch(err=>{
        console.log(err)
    })
}

const MainWithAsync = async () =>{
    const data = await getDataFromServer()
    console.log(data)
}

//Main()
//MainWithCallbacks()
//MainWithPromises()
//MainWithAsync()


