/**
 * Calling functions from different contexts by passing them into other
 * functions as parameters is one of the primary ways to simulate
 * async programming in Javascript
 * 
 */

const AddTwoNumbers = (num1,num2) =>{
    return num1+num2
}

const MultiplyTwoNumbers = (num1,num2) =>{
    return num1*num2
}

const LogMathFunction = (mathFunction,num1,num2) =>{
    let result = mathFunction(num1,num2)
    console.log(result)
}

const CreateLogMyParamsFunction = (param1,param2) =>{
    return ()=>console.log(param1,param2)
}

//LogMathFunction(AddTwoNumbers,3,8)
//let thisFunction = CreateLogMyParamsFunction('hello','world')
