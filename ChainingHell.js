const { getDataFromServer, alterData, transformData } = require("./AsyncFunctions")

const ChainingHell = () =>{

    getDataFromServer().then(server_result=>{

        transformData(server_result).then(transformed_data=>{

            alterData(transformed_data).then(altered_data=>{
                console.log(altered_data)
            }).catch(err=>{
                console.log(`There was an error altering server data: ${err}`)
            })

        }).catch(err=>{
            console.log(`There was an error transforming server data: ${err}`)
        })
    }).catch(err=>{
        console.log(`There was an getting server data: ${err}`)
    })
}

ChainingHell()