
/**
 * A promise takes a function as its parameter
 * The parameter function has two arguements (resolve and reject) *These are just parameters they can be named anything
 * resolve and reject are both functions themselves, that you call when a task has completed or a task has failed
 */

const promiseShouldFail = true

/**
 * When "consuming" a promise you can 'chain' the 'then' function and/or the 'catch' function
 * to capture when resolve/reject are called
 */

const Main = ()=>{

    let MyPromise = new Promise((resolve,reject)=>{ 
        setTimeout(()=>{
            if(promiseShouldFail){
                reject() /*reject(`I failed`) || reject({message: `There was an error`}) */
            }else{
                resolve() /*resolve(`Task successful`) || resolve({message: `Completed Task!`}) */
            }
        },300) //setTimeout takes in a function and an amount of time to wait before calling that function
    })


    MyPromise.then(res=>{
        console.log('Promise resolved')
    }).catch(err=>{
        console.log('Promise failed')
    })
}

