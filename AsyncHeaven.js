const { getDataFromServer, transformData, alterData } = require("./AsyncFunctions")

const AsyncHeaven = async () =>{
    let data = await getDataFromServer()
    let transformedData = await transformData(data)
    let alteredData = await alterData(transformedData)
    console.log(alteredData)
}

AsyncHeaven()