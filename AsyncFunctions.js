
const data = [
    {id: 1, name: 'james'},
    {id: 2, name: 'nick'},
    {id: 3,name: 'steve'},
    {id: 4, name: 'john'}
]

module.exports.getDataFromServer = async () =>{
    await new Promise((res,rej)=>setTimeout(res,3000))
    console.log('Data recieved!')
    return data
}
module.exports.transformData = async (data) =>{
    await new Promise((res,rej)=>setTimeout(res,3000))
    console.log('Data transformed!')
    return data.map(data=>({...data,newProperty: 'I was transformed!'}))
}
module.exports.alterData = async (data) =>{
    await new Promise((res,rej)=>setTimeout(res,3000))
    console.log('Data altered!')
    return data.map(data=>({...data,anotherNewProperty: 'I was altered!'}))
}

module.exports.getDataWithCallback = async (callback) =>{
    await new Promise((res,rej)=>setTimeout(res,3000))
    console.log('Data recieved!')
    callback(data)
}

module.exports.transformDataWithCallback = async (data,callback) =>{
    await new Promise((res,rej)=>setTimeout(res,3000))
    console.log('Data transformed!')
    callback(data.map(data=>({...data,newProperty: 'I was transformed!'})))
}

module.exports.alterDataWithCallback = async (data,callback) =>{
    await new Promise((res,rej)=>setTimeout(res,3000))
    console.log('Data altered!')
    callback(data.map(data=>({...data,anotherNewProperty: 'I was altered!'})))
}
