const { getDataWithCallback, transformDataWithCallback, alterDataWithCallback } = require("./AsyncFunctions")

const CallbackHell = () =>{

    const callThisWhenDone = (data) =>{

        const callThisWhenDoneWithThat = (transformed) =>{

            const callThisAfterDoingThatAndTheOther = (altered) =>{
                console.log(altered)
            }

            alterDataWithCallback(transformed,callThisAfterDoingThatAndTheOther)
        }

        transformDataWithCallback(data,callThisWhenDoneWithThat)

    }

    getDataWithCallback(callThisWhenDone)
}

CallbackHell()